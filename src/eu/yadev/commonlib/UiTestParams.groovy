package eu.yadev.commonlib;

class UiTestParams extends Params {
  String url = null;
  String dParams = "";
  String actions = "clean test";
  String testResultsLocation = "target/surefire-reports/**/*.xml";
  String artifacts = "";
  boolean failStep = true;
  
  int minPort = 1024;
  int maxPort = 9999;

  private static UiTestParams SELF;

  public static UiTestParams createWith(Map args) {
    SELF = new UiTestParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static UiTestParams get() {
      return SELF;
  }

  public hasUrl() {
    return (this.url != null && !this.url.trim().equals(""));
  }
}
