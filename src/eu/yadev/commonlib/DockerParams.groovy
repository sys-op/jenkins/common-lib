package eu.yadev.commonlib;

class DockerParams extends Params {
  String registry = null;
  String repo = null;
  String[] tags = null;

  private static DockerParams SELF;

  public static DockerParams createWith(Map args) {
    SELF = new DockerParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static DockerParams get() {
      return SELF;
  }

  public boolean hasTags() {
    return (this.tags != null && this.tags.size() > 0);
  }
  public boolean hasRepo() {
    return (this.repo != null && !this.repo.trim().equals(""));
  }

  public boolean hasRegistry() {
    return (this.registry != null && !this.registry.trim().equals(""));
  }

  protected void applyTagLogic() {
    if(! hasTags()) {
      throw new RuntimeException("'tags' param must be set");
    }

    if(hasRepo()) {
      List<String> tagsPlusRepo = new ArrayList<>();
       this.tags.each {
          String fullTag = "";
          if(hasRegistry()) {
             fullTag = "${this.registry}"+"/";
          }
          fullTag += this.repo+":"+"${it}";
          tagsPlusRepo.add(fullTag);
       }
       this.tags = tagsPlusRepo as String[];
    } else {
      throw new RuntimeException("'repo' param must be set");
    }
  }
}
