package eu.yadev.commonlib;

class ShellParams extends Params {
  String encoding = null;
  String label = null;
  boolean returnStatus = false;
  boolean returnStdout = false;

  private static ShellParams SELF;

  public static ShellParams createWith(Map args) {
    SELF = new ShellParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static ShellParams createWith(Params p) {
    SELF = new ShellParams();
    SELF.updateValues(p.properties);
    return SELF;
  }

  public static ShellParams get() {
      return SELF;
  }
}
