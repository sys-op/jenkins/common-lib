package eu.yadev.commonlib;

class DockerLogoutParams extends DockerParams {
  String server = null;

  private static DockerLogoutParams SELF;

  public static DockerLogoutParams createWith(Map args) {
    SELF = new DockerLogoutParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static DockerLogoutParams get() {
      return SELF;
  }
}
