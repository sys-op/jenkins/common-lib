package eu.yadev.commonlib;

class SwarmDeployParams extends Params {

  String hookUrl = null;

  private static SwarmDeployParams SELF;

  public static SwarmDeployParams createWith(Map args) {
    SELF = new SwarmDeployParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static SwarmDeployParams get() {
      return SELF;
  }

  public hasHookUrl() {
    return (this.hookUrl != null && !this.hookUrl.trim().equals(""));
  }
}
