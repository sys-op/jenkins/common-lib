package eu.yadev.commonlib;

class KubeDeployParams extends Params {

    public static final DEFAULT_NAMESPACE = "default";
    private static final DEFAULT_TAG = "latest";
    private static final DEFAULT_CONTAINER_NAME = "app";

    String namespace = DEFAULT_NAMESPACE;
    String workloadName = null;
    String containerName = DEFAULT_CONTAINER_NAME;
    String imageRepo = null;
    String imageTag = DEFAULT_TAG;

    private static KubeDeployParams SELF;

    public static KubeDeployParams createWith(Map args) {
        SELF = new KubeDeployParams();
        SELF.updateValues(args);
        return SELF;
    }

    public static KubeDeployParams get() {
        return SELF;
    }

    public boolean hasWorkloadName() {
        return (this.workloadName != null && !this.workloadName.trim().equals(""));
    }

    public boolean hasImageRepo() {
        return (this.imageRepo != null && !this.imageRepo.trim().equals(""));
    }

}
