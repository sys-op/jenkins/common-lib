package eu.yadev.commonlib;

class DockerBuildParams extends DockerParams {
  String dockerFile = "Dockerfile";
  String buildContext = ".";
  String[] buildArgs = null;

  private static DockerBuildParams SELF;

  public static DockerBuildParams createWith(Map args) {
    SELF = new DockerBuildParams();
    SELF.updateValues(args);
    SELF.applyTagLogic();
    return SELF;
  }

  public static DockerBuildParams get() {
      return SELF;
  }

  public boolean hasArgs() {
    return (this.buildArgs != null && this.buildArgs.size() > 0);
  }
}
