package eu.yadev.commonlib;

class VaadinParams extends Params {
  String prodModeProfile = null;
  String extraProfiles = null;
  String runSiteTarget = null;

  String propertiesLocation = "src/main/resources";

  private static VaadinParams SELF;

  public static VaadinParams createWith(Map args) {
    SELF = new VaadinParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static VaadinParams get() {
      return SELF;
  }
}
