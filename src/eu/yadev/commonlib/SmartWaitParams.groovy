package eu.yadev.commonlib;

class SmartWaitParams extends Params {
  String url = null;
  int tries = 3;
  int timeout = 10;

  private static SmartWaitParams SELF;

  public static SmartWaitParams createWith(Map args) {
    SELF = new SmartWaitParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static SmartWaitParams get() {
      return SELF;
  }

  public hasUrl() {
    return (this.url != null && !this.url.trim().equals(""));
  }
}
