package eu.yadev.commonlib;
class Params {
  boolean verbose = false;
  String label = null;
  String script = null; //this is for passing params to shell function

  private static Params SELF;

  public static Params get() {
      return SELF;
  }

  protected void updateValues(Map args) {
    if(args == null) {return; }
    args.each { key, value ->
      if(this.hasProperty(key) && !key.equals("class")) {
        this."${key}" = value;
      } //avoid 'class' property
    }
  }

  public String verbose() {
    if(this.verbose) {
      return 'set -x'
    } else {
      return 'set +x'
    }
  }

  public void applyDefaultLabel(String defaultLabel) {
    if(this.label == null || this.label.trim().equals("")) {
      this.label = defaultLabel;
    }
  }

  public String toString() {
    String str = ''
    this.properties.each { p ->
      str += "\n${p}"
    }
      return str;
    }
}
