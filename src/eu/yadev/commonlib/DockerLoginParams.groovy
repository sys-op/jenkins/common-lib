package eu.yadev.commonlib;

class DockerLoginParams extends DockerParams {
  String creds = null;
  String server = null;

  private static DockerLoginParams SELF = null;

  public static DockerLoginParams createWith(Map args) {
    SELF = new DockerLoginParams();
    SELF.updateValues(args);
    return SELF;
  }

  public static DockerLoginParams get() {
      return SELF;
  }
}
