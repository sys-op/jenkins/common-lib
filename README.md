# common-lib

Common Jenkins Library

## Common Params
Params, which can apply to every function.

### Params
* verbose - if true, output shows every command + output, otherwise - only output
* label - string, which is located before job's command. Type: String

## Shell
Same as sh command, but  with verbose support

### Params
* script - command to run. Type: String
* encoding - command encoding. Type: String
* returnStatus - same as in sh. Type: boolean
* returnStdout - same as in sh. Type: boolean

### Example
```
shell(script: 'echo Hello', encoding: 'UTF-8', returnStatus: false, returnStdout: false, verbose: true, label: 'Example Label')
```

## Vaadin
Builds Vaadin apps with maven

### Params
* prodModeProfile - name of profile in `pom.xml`, which triggers build in Vaadin Production mode. If empty or absent - just build in Dev Mode
* extraProfiles - list of comma separated profiles to activate as well.
* propertiesLocation - location of application.properties. Default: `src/main/resources`

### Example
```
vaadin(prodModeProfile: 'production-mode', extraProfiles: 'noTesting')
```

### Extra
Vaadin Production Mode profile example:
```
<profile>
    <id>production-mode</id>

    <dependencies>
        <dependency>
            <groupId>com.vaadin</groupId>
            <artifactId>flow-server-production-mode</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.vaadin</groupId>
                <artifactId>vaadin-maven-plugin</artifactId>
                <version>${vaadin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>copy-production-files</goal>
                            <goal>package-for-production</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</profile>
```

## Docker
Params common to all docker functions

### Params
* registry - custom docker registry. Not needed if DockerHub used.
* repo - Docker repository. Usually is 'owner/product' format used.
* tags - docker tags to apply during the build. Should be array of nonEmpty strings
* tag - tag to apply

## Docker Build
Triggers `docker build` command.

### Params
* dockerfileDir - nonEmpty string with path, where Dockerfile located. This is optional, since default is '.', which is common practice almost everywhere

### Example
```
dockerBuild(registry: 'docker.io',repo: 'me/hello', tags: ["latest","0.1"], dockerfileDir: 'dirWithDockerfile');
```

## Docker Login
Performs log in to docker repository

### Params
* creds - Jenkins credentialsId to log to docker repository in
* server - repository host and port, if not DockerHub used.

### Example
```
dockerLogin(creds: 'my-hub-creds', server: 'myhub:9000');
```

## Docker Logout
Performs log out from docker repository

### Params
* server - repository host and port, not needed if DockerHub used. If not set, will use same server at used in `dockerLogin` or `DockerHub`

### Example
```
dockerLogout(server: 'myhub:9000');
```

## Docker Push
Performs push to remote repo. You have log in first.

### Example
```
dockerPush();
```

## Docker Clean
Performs local system cleanup by executing `docker rmi image`

### Example
```
dockerClean();
```

## Deploy to Swarm
Triggers Portainer Service webhook, which redeploys service  

### Example

```
deployToSwarm(hookUrl: "https://swarm.site/api/webhooks/45467474");
```

### Params
* hookUrl - valid URL with Portainer Service webhook

## Smart Wait
Waiting until application restarts

### Example
```
smartWait(url: url, tries: 3, timeout: 10);
```

### Params
* url - application URL
* tries - number of attempts. Default: 3
* timeout - time between attempts in seconds. Default: 10 seconds.

## Test App
Launching `mvn clean test` with app URL to test. Application launching at random port in range (1024-9999).
Range is adjustable by user.

### Example
```
testApp(url: url, minPort: 1234, maxPort: 2345);
```

### Params
* url - URL of application to test
* minPort - min port for launching test App
* maxPort - max port for launching test App

## DeployToKube
Deploys to kubernetes. Updates currently existing deployment: sets a new image and restarts deployment.  

### Params
* namespace - Namespace of deployment (default: `default`)
* workloadName - Name of deployment (aka workload). 
* imageRepo - Docker image repo. Example: `myname/myimage`
* imageTag - Docker image tag. This is normally same tag, that was pushed to DockerHub. (default: `latest`)

```
deployToKube(
  namespace: 'my-namespace-in-kubernetes', 
  workloadName: 'my-deployment', 
  imageRepo: 'me/myrepo', imageTag: '0.0'
)
```