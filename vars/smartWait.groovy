import eu.yadev.commonlib.SmartWaitParams;

def call(Map args) {
  SmartWaitParams params = SmartWaitParams.createWith(args);
  params.applyDefaultLabel("Waiting for service to restart");

  URL url;
  if(!params.hasUrl()) {
     throw new RuntimeException("'url' param is missing");
  } else {
    try {
      url = new URL(params.url);
    } catch (Exception e) {
      throw new RuntimeException("given 'url' param contains not valid URL");
    }
  }

  String cmd = "wget -q --tries=${params.tries} --timeout=${params.timeout} --spider ${url}"
  params.script = cmd;
  shell(params);
}
