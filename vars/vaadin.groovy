import eu.yadev.commonlib.VaadinParams;

def call(Map args) {
  VaadinParams v = VaadinParams.createWith(args); //should be above all
  v.applyDefaultLabel("Build Vaadin App");
  if(v.verbose) echo "Params = ${v.toString()}"

  List<String> profiles = new ArrayList<>();
  List<String> targets = new ArrayList<>();

  targets.add("clean");
  targets.add("package");
  
  if(v.runSiteTarget != null && !v.runSiteTarget.trim().equals("") && v.runSiteTarget.trim().equalsIgnoreCase("true")) {
    targets.add("site");
  }

  boolean prodMode = false;

  if(v.prodModeProfile != null && !v.prodModeProfile.trim().equals("")) {
    profiles.add(v.prodModeProfile);
    prodMode = true;
    v.applyDefaultLabel("Build Vaadin App in Prod Mode");
  }
  
  if(v.extraProfiles != null && !v.extraProfiles.trim().equals("")) {
    v.extraProfiles.split(",").each {
      profiles.add("${it}");
    }
  }

  if(v.verbose) echo "Profiles = ${profiles.toString()}"

  String command = "mvn";
  targets.each {
    command += " ${it}";
  }

  if(profiles.size() > 0) {
    command += " -P ";
    profiles.each {
      command += "${it},"
    }
  }

  if(command.endsWith(",")) {
    command = command.substring(0, command.length() -1).trim(); //removing last ','
  }

  v.script = command;
  shell(v);
}

