import eu.yadev.commonlib.SwarmDeployParams;

def call(Map args) {
  String NO_URL_MARKER = "noUrl";

  SwarmDeployParams params = SwarmDeployParams.createWith(args);
  params.applyDefaultLabel("Deploy to Docker Swarm");
  
  URL hookUrl;
  if(!params.hasHookUrl()) {
     throw new RuntimeException("'hookUrl' param is missing");
  } else {
    if(params.hookUrl.equals(NO_URL_MARKER)) {
      hookUrl = NO_URL_MARKER;
    } else {
      try {
        hookUrl = new URL(params.hookUrl);
      }catch (Exception e) {
        throw new RuntimeException("given 'hookUrl' param contains not valid URL");
      }
    }
  }

  if(hookUrl.equals(NO_URL_MARKER)) {
      echo "Hook not implemented yet";
  } else {
      String cmd = "curl -X POST -H \"Content-Type: application/json\" ${hookUrl}";
      params.script = cmd;
      shell(params);
  }
}
