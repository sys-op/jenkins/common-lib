import eu.yadev.commonlib.DockerParams;
import eu.yadev.commonlib.DockerBuildParams;

def call(Map args) {
  DockerParams params = DockerParams.createWith(args);
  DockerParams buildParams = DockerBuildParams.get();

  if(!params.hasRepo() && !params.hasTags() && buildParams != null) {
    reuseBuildParams(params, buildParams);
  } else {
    params.applyTagLogic();
  }

  params.tags.each {
    String cmd = "docker push ${it}";
    params.script = cmd;

    params.applyDefaultLabel("Docker Push - ${it}");
    shell(params);
    params.label = null; //otherwise same label will apply
  }
}

def reuseBuildParams(DockerParams params, DockerParams buildParams) {
  if(buildParams != null) {
    if(! params.hasRegistry()) {
      if(buildParams.hasRegistry()) {
          params.registry = buildParams.registry;
      }
    }

    if(! params.hasRepo()) {
      if(buildParams.hasRepo()) {
          params.repo = buildParams.repo;
      }
    }

    if(! params.hasTags()) {
      if(buildParams.hasTags()) {
          params.tags = buildParams.tags;
      }
    }
  }
}
