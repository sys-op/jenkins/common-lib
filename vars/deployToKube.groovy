import eu.yadev.commonlib.KubeDeployParams
def call(Map args) {

    KubeDeployParams params = KubeDeployParams.createWith(args);
    params.applyDefaultLabel("Deploy to Kubernetes");


    if(!params.hasWorkloadName()) {
        throw new RuntimeException("'workloadName' param is missing");
    }
    if(!params.hasImageRepo()) {
        throw new RuntimeException("'imageRepo' param is missing");
    }

    String setImageCommand = "kubectl";
    if(params.namespace != KubeDeployParams.DEFAULT_NAMESPACE) {
        setImageCommand += " -n ${params.namespace}";
    }

    setImageCommand += " set image deployment/${params.workloadName}";
    setImageCommand += " ${params.containerName}=${params.imageRepo}:${params.imageTag}";
    setImageCommand += " --record";

    String rolloutRestartCommand = "kubectl";
    if(params.namespace != KubeDeployParams.DEFAULT_NAMESPACE) {
        rolloutRestartCommand += " -n ${params.namespace}";
    }

    rolloutRestartCommand += " rollout restart deployment/${params.workloadName}";

    sh setImageCommand;
    sh rolloutRestartCommand;

}
