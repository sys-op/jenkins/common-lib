import eu.yadev.commonlib.DockerLogoutParams;
import eu.yadev.commonlib.DockerLoginParams;
def call(Map args) {
  DockerLogoutParams params = DockerLogoutParams.createWith(args);
  params.applyDefaultLabel("Docker Logout");
  String cmd = "docker logout";

  if(params.server != null && !params.server.trim().equals("")) {
    cmd += " ${params.server}";
  } else {
    DockerLoginParams loginParams = DockerLoginParams.get();
    if(loginParams != null && loginParams.server != null && !loginParams.server.trim().equals("")) {
      cmd += " ${loginParams.server}";
    }
  }
  params.script = cmd;
  shell(params);

}
