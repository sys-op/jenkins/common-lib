import eu.yadev.commonlib.UiTestParams;

def call(Map args) {
  UiTestParams params = UiTestParams.createWith(args);

  int uiPort = uiPort(params);
  String testUrl = params.url;

  params.applyDefaultLabel("UI Tests for ${testUrl}");

  while(true) {
    if(isPortBusy(uiPort)) {
        uiPort = uiPort(params);
    } else {
        break;
    }
  }

  def testFailed = false;
  try {
    sh "mvn -Dport=${uiPort} -Dtest.url=$testUrl ${params.dParams} ${params.actions}"
  } catch (err) {
    //just continue to results instead of failing build just mark build as failed
    testFailed = true;
  }

  //results
  junit(testResults: "${params.testResultsLocation}", allowEmptyResults: true);
  
  def artifactsToArchive = "${params.artifacts}";
  if(artifactsToArchive?.trim()) {
    archiveArtifacts(artifacts: artifactsToArchive, allowEmptyArchive: true);
  }

  //def failStep = ${params.failStep}
  
  //if(failStep == false) {
  //  echo "Stage failed, but we ignore it";
  //  currentBuild.result = 'SUCCESS';
  //  return;
  //}

  //if(testFailed) {
  //   error("Tests failed")
  //}
    
}

def int uiPort(UiTestParams params) {
  Random rnd = new Random();
  if(params.minPort < 1024) {
    throw new RuntimeException("'minPort' should be above 1024 to allocate with non-root user");
  }
  if(params.minPort > 65535) {
    throw new RuntimeException("'minPort' should be below 65535");
  }
  if(params.maxPort > 65535) {
    throw new RuntimeException("'maxPort' should be below 65535");
  }
  int rPort =  rnd.nextInt(params.maxPort);
  if(rPort <= params.minPort) {
    return rPort + params.minPort;
  } else {
    return rPort;
  }
}

def boolean isPortBusy(int port) {
  String url = "http://localhost:${port}";
  String cmd = "wget -q --spider ${url}"
  int status = sh(script: cmd, returnStatus: true);
  return (status == 0);
}
