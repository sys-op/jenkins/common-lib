import eu.yadev.commonlib.DockerBuildParams;

def call(Map args) {
  DockerBuildParams p = DockerBuildParams.createWith(args);
  p.applyDefaultLabel("Docker Build");

  if(p.tags.size() == 0) { throw new RuntimeException("No tags found");}

  StringBuilder sb = new StringBuilder("docker build ");
  if(p.hasArgs()) {
    p.buildArgs.each {
      sb.append("--build-arg ${it} ");
    }
  }
  p.tags.each {
    sb.append("-t ${it} ");
  }
  sb.append("-f ").append(p.dockerFile).append(" ");
  sb.append(p.buildContext);

  p.script = sb.toString();
  shell(p);
}
