import eu.yadev.commonlib.ShellParams;
import eu.yadev.commonlib.Params;

def call(Object args) {
  //for Params object
  if(args instanceof Params) {
    args = args.properties;
  }
  if(!(args instanceof Map)) {
    throw new RuntimeException("Unsupported arg. Should 'Map' or 'Params' instance only");
  }

  ShellParams p = ShellParams.createWith(args);

  String NEW_LINE = System.lineSeparator();
  String cmd;
  if(p.verbose) {
    cmd = "set -x" + NEW_LINE + p.script;
  } else {
    cmd = "set +x" + NEW_LINE + p.script;
  }


  Map shParams = new HashMap<Object, Object>();
  if(p.script != null && ! p.script.trim().equals("")) {
    shParams.put("script", cmd);
  } else {
    throw new RuntimeException("Nothing to run");
  }

  if(p.encoding != null && ! p.encoding.trim().equals("")) {
    shParams.put("encoding", p.encoding);
  }

  if(p.label != null && ! p.label.trim().equals("")) {
    shParams.put("label", p.label);
  }

  shParams.put("returnStatus", p.returnStatus);
  shParams.put("returnStdout", p.returnStdout);

  sh(shParams);
}
