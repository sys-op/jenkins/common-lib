import eu.yadev.commonlib.DockerLoginParams;

def call(Map args) {
  DockerLoginParams p = DockerLoginParams.createWith(args);
  p.applyDefaultLabel("Docker Login");
  //docker login [server]
  if(p.creds == null || p.creds.trim().equals("")) {
    throw new RuntimeException("'creds' param missing");
  }
  String cmd;
  withCredentials([[ $class: 'UsernamePasswordMultiBinding', credentialsId: p.creds,
                     usernameVariable: 'USR', passwordVariable: 'PASS'
  ]]) {
        //new ShellCommandRunner().runCmd(Docker.login(env.USR, env.PASS));
        cmd = "echo ${env.PASS} | docker login -u ${env.USR} --password-stdin";
        if(p.server != null && !p.server.trim().equals("")) {
           cmd += " ${p.server}";
        }

        p.script = cmd;
        shell(p);
    }


}
